resource "aws_cloudwatch_log_group" "logGroup" {
  name = "ServerLogs"
}

resource "aws_iam_role" "ecs_task_exec_role" {
  name = "ecsTaskExecRole"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
    {
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid = ""
      Principal = {
      Service = "ecs-tasks.amazonaws.com"
     }    
    },
    ]
  })
}



module "satisfactoryServer" {
  source = "./server_module"
  server_name = "satisfactory"
  clusterName = "satisfactoryCluster"
  serviceName = "satisfactoryService"
  taskName = "satisfactoryTask"
  ecsExecRoleArn = aws_iam_role.ecs_task_exec_role.arn
  containerCPU = 2048
  containerRAM = 8192
  ingressUDP = [7777,15000,15777]
  containerDefinition = jsonencode([
   {
    name = "server"
    image = "wolveix/satisfactory-server:latest"
    cpu = 2048
    memory = 8192
    essential = true
    logConfiguration = {
      logDriver = "awslogs"
      options = {
        awslogs-region = "ca-central-1"
        awslogs-group = "ServerLogs"
        awslogs-stream-prefix = "SatisfactoryServer"
      }
    }
    environment = [
    {
     name = "STEAMBETA"
     value = "true"
    },
    ]
    mountPoints = [
     {
      sourceVolume = "Volume"
      containerPath = "/config"
     },
    ]
    portMappings = [
     {containerPort=7777},
     {containerPort=15000},
     {containerPort=15777}
    ]
   }
  ])
  additional_tags = {
    workload = "satisfactory"
  }
}


module "valheimServer" {
  source = "./server_module"
  server_name = "valheim"
  clusterName = "valheimCluster"
  serviceName = "valheimService"
  taskName = "valheimTask"
  ecsExecRoleArn = aws_iam_role.ecs_task_exec_role.arn
  containerCPU = 2048
  containerRAM = 4096
  ingressUDP = [2456,2457,2458]
  containerDefinition = jsonencode([
   {
    name = "server"
    image = "lloesche/valheim-server:latest"
    cpu = 2048
    memory = 4096
    essential = true
    logConfiguration = {
      logDriver = "awslogs"
      options = {
        awslogs-region = "ca-central-1"
        awslogs-group = "ServerLogs"
        awslogs-stream-prefix = "ValheimServer"
      }
    }
    environment = [
    {
     name = "SERVER_NAME"
     value = "SchroTech Valheim"
    },
    {
     name = "SERVER_PASS"
     value = "friend"
    },
    {
     name = "STEAMCMD_ARGS"
     value = "validate"
    },
    {
     name = "WORLD_NAME"
     value = "SpecialCircumstances"
    },
    ]
    mountPoints = [
     {
      sourceVolume = "Volume"
      containerPath = "/config"
     },
    ]
    portMappings = [
     {containerPort=2456},
     {containerPort=2457},
     {containerPort=2458}
    ]
   }
  ])
  additional_tags = {
    workload = "valheim"
  }
}
