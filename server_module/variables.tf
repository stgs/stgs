variable "server_cidr_block" {
  type = string
  description = "CIDR Block for Server VPC"
  default = "10.10.0.0/16"
}
variable "server_subnet_cidr" {
  type = string
  description = "CIDR Block for Server Subnet"
  default = "10.10.1.0/24"
}
variable "server_name" {
  type = string
  description = "Name for this server application"
  default = "gameserver"
}
variable "loggroupName" {
  type = string
  description = "Name for this server log group"
  default = "ServerLogs"
}
variable "additional_tags" {
  type = map(string)
  description = "Additional tags for created resources"
  default = {}
}
variable "clusterName" {
  type = string
  description = "Name for this server ECS Cluster"
}
variable "serviceName" {
  type = string
  description = "Name for this server ECS Cluster Service"
}
variable "taskName" {
  type = string
  description = "Name for this server ECS Cluster Service Task"
}
variable "containerDefinition" {
  type = string
  description = "valid Container Definition"
}
variable "ecsExecRoleArn" {
  type = string
  description = "ARN For the ECS Execution Role"
}
variable "containerCPU" {
  type = number
  description = "Container CPU Setting"
}
variable "containerRAM" {
  type = number
  description = "container RAM Setting"
}
variable "ingressUDP" {
  type = list(number)
  description = "List of inbound UDP ports"
}
