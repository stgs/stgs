resource "aws_efs_file_system" "serverFiles" {
  tags = merge(
    var.additional_tags,
    {
      purpose = "gameserver"
    },
  )
}

resource "aws_vpc" "server_vpc" {
  cidr_block = var.server_cidr_block
  enable_dns_hostnames = "true"
}

resource "aws_subnet" "server_subnet" {
  vpc_id = aws_vpc.server_vpc.id
  cidr_block = var.server_subnet_cidr
}

resource "aws_security_group" "serverSecurityGroup" {
  name = "Server Security Group"
  description = "Security Group for Server"
  vpc_id = aws_vpc.server_vpc.id
  dynamic ingress {
    for_each = var.ingressUDP
    content {
      from_port = ingress.value
      to_port = ingress.value
      cidr_blocks = ["0.0.0.0/0"]
      protocol = "udp"
    }
  }

  ingress {
    description = "Self Allow"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self = "true"
  }
  egress {
    description = "Self Allow"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_efs_mount_target" "serverFileMountTarget" {
  file_system_id = aws_efs_file_system.serverFiles.id
  subnet_id = aws_subnet.server_subnet.id
  security_groups = [aws_security_group.serverSecurityGroup.id]
}
resource "aws_internet_gateway" "serverInternetGateway" {
  vpc_id = aws_vpc.server_vpc.id
}
resource "aws_route_table" "serverRouteTable" {
  vpc_id = aws_vpc.server_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.serverInternetGateway.id
  }
}
resource "aws_route_table_association" "serverRouteTableAssc" {
  subnet_id = aws_subnet.server_subnet.id
  route_table_id = aws_route_table.serverRouteTable.id
}
resource "aws_ecs_cluster" "serverCluster" {
  name = var.clusterName
  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"
      log_configuration {
        cloud_watch_log_group_name = var.loggroupName
      }
    }
  }
  tags = merge(
    var.additional_tags,
    {
      purpose = "gameserver"
    },
  )
}
resource "aws_ecs_service" "serverService" {
  name = var.serviceName
  launch_type = "FARGATE"
  cluster = aws_ecs_cluster.serverCluster.id
  task_definition = aws_ecs_task_definition.serverServiceTaskDefinition.id
  desired_count = 1
  lifecycle {
    ignore_changes = [desired_count]
  }
  network_configuration {
    subnets = [aws_subnet.server_subnet.id]
    security_groups = [aws_security_group.serverSecurityGroup.id]
    assign_public_ip = "true"
  }
  tags = merge(
    var.additional_tags,
    {
      purpose = "gameserver"
    },
  )
}
resource "aws_ecs_task_definition" "serverServiceTaskDefinition" {
  family = var.taskName
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn = var.ecsExecRoleArn
  cpu = var.containerCPU
  memory = var.containerRAM
  container_definitions = var.containerDefinition
  volume {
    name = "Volume"
    efs_volume_configuration {
      file_system_id = aws_efs_file_system.serverFiles.id
    }
  }
  tags = merge(
    var.additional_tags,
    {
      purpose = "gameserver"
    },
  )
}


